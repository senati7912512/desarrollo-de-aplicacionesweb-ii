# DESARROLLO DE APLICACIONESWEB II 

Programas necesarios:

* [Laragon](https://drive.google.com/file/d/11wHfqHh3o28HyAKTO8bJWPn758hgHofX/edit)
* [Composer](https://drive.google.com/file/d/1H8UAeAbLBDla4emoWVVAcZ5p6vEiQdXl/edit)

Material de consulta:

* [Libro PHP](https://drive.google.com/file/d/1t6zH7M7McT8QT1Fy21rwS1F8cv7WMWAL/edit)
* [Libro II PHP](https://drive.google.com/file/d/1RKYYYBqPYRnp5_KwqZwvHpBvjHmRULw2/edit)
* [El gran libro de Angular](https://drive.google.com/file/d/1-SE4oDGKnyLDzkzBpuSRQIgWpNLrGAzR/edit)
* [Material El gran libro de Angular](https://drive.google.com/file/d/13t46kqhAFUHPeSNqtuCo7jeVIB-y9QId/edit)

Presentaciones:

* [PHP y MySQL](https://docs.google.com/presentation/d/1D_L54okDtl54Xd2iz3gDBnWDPRG5uZ7b/edit)

## Pasos para instar laragon

1. Descargar el programa del link.
2. Ejecutar laragon.
3. Guardar todos los programas a desarrollar en la carpeta www.
4. Configurar PHP en la variable de entorno del sistema.
5. Instalar composer. [Descargar](https://getcomposer.org/)
