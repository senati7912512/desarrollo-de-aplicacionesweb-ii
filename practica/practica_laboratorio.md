# Crearemos un sistema de control de stock

## Pado 01: Crear la base de datos stock

```sql
CREATE TABLE productos (
    id INT NOT NULL AUTO_INCREMENT,
    descripcion VARCHAR(50) UNIQUE NOT NULL,
    stock_minimo INT NOT NULL,
    stock_maximo INT NOT NULL,
    PRIMARY KEY (id)
);

insert into productos (descripcion, stock_minimo, stock_maximo) values 
('Banana', 10, 100);

CREATE TABLE compras (
    id INT NOT NULL AUTO_INCREMENT,
    producto_id INT NOT NULL,
    cantidad INT NOT NULL,
    precio DECIMAL(9,2) NOT NULL,
    data DATE NULL DEFAULT NULL,
    PRIMARY KEY (id),
 constraint compra_fk foreign key (producto_id) references productos(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE stock (
    id INT NOT NULL AUTO_INCREMENT,
    producto_id INT NOT NULL,
    cantidad INT NOT NULL,
    PRIMARY KEY (id),
 constraint stock_fk foreign key (producto_id) references productos(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE ventas (
    id INT NOT NULL AUTO_INCREMENT,
    producto_id INT NOT NULL,
    cantidad INT NOT NULL,
    data DATE NULL DEFAULT NULL,
    precio DECIMAL(9,2) NOT NULL,
    PRIMARY KEY (id),
 constraint venta_fk foreign key (producto_id) references productos(id) ON DELETE CASCADE ON UPDATE CASCADE
);
```

## Paso 02: Crear una carpeta con el nombre stock dentro de la carpeta www

## Paso 03: Crear el archivo conexion.php

```php
<?php
$hostname = "localhost";
$username = "root";
$password = "";
$database = "stock";
$row_limit = 8;
$sgbd = 'mysql'; // mysql, pgsql

// conexion to mysql
try {
    $pdo = new PDO($sgbd.":host=$hostname;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $err) {
    die("Error! " . $err->getMessage());
}
```

## Paso 04: Agregar el archivo assets en la carpeta stock [assets](https://drive.google.com/file/d/1uoQ_Tw9Y_SPZU-lPX1MbrWOokUhuwpu9/edit)

## Paso 05: Crear el archivo index.php

```php
<?php require_once 'header_idx.php'; ?>

        <table class="table">
            <tr>
                <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><a href="productos">productos</a></td>
                <td><a href="compras">Compras</a></td>
                <td><a href="ventas">ventas</a></td>
                <td><a href="stock">stockheader_idx.php
                
                </a></td>
            </tr>
        </table>
    </div>
</div>

<?php require_once 'footer.php'; ?>
```

## Paso 06: Crear el archivo header_idx.php

```php
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>stock Simplificado en PHP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <style type="text/css">
    .panel-footer {
        padding: 0;
        background: none;
    }
    </style>
</head>

<body>
<br/>
<div class="container">
    <div class="panel panel-default">
        <div class="jumbotron text-center"><h1><strong>stock Simplificado en PHP</strong></h1></div>
```

## Paso 07: Crear el archivo footer.php

```php
<style>
footer {
    text-align:center;
    left: 0px;
    position:fixed;
    bottom:0px;
    width:100%;
}
</style>

<footer>
    <i>Sistema diseñano por<a href="https://ribamar.net.br" target="_blank">Estudiantes SENATI</a></i>
</footer>

</body>
</html>
```

## Paso 08: Crear la carpeta productos

## Paso 09: Crear dentro de la carpeta productos el archivo index.php

```php
<?php
include_once("../conexion.php");

$stmt = $pdo->prepare("SELECT COUNT(*) FROM productos");
$stmt->execute();
$rows = $stmt->fetch();

// get total no. of pages
$total_pages = ceil($rows[0]/$row_limit);

$operacao = 'productos';

require_once '../header.php';
?>

        <div class="row">
            <!-- Adicionar registro -->
            <div class="text-left col-md-2 top">
                <a href="./add.php" class="btn btn-primary pull-left">
                    <span class="glyphicon glyphicon-plus"></span> Adicionar
                </a>
            </div>

            <!-- Form de busca-->
            <div class="col-md-10">
                <form action="./search.php" method="get" >
                  <div class="pull-right top">
                  <span class="pull-right">  
                    <label class="control-label" for="palavra" style="padding-right: 5px;">
                      <input type="text" value="" placeholder="Descripcion ou parte" class="form-control" name="keyword">
                    </label>
                    <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Busca</button>&nbsp;
                  </span>                 
                  </div>
                </form>
            </div>
     </div>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>stock mínimo</th>
                    <th>stock máximo</th>
                </tr>
            </thead>
            <tbody id="pg-results">
            </tbody>
        </table>
        <div class="panel-footer text-center">
            <div class="pagination"></div>
        </div>
    </div>
</div>
    
<script src="../assets/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.bootpag.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#pg-results").load("fetch_data.php");
    $(".pagination").bootpag({
        total: <?=$total_pages?>,
        page: 1,
        maxVisible: 18,
        leaps: true,
        firstLastUse: true,
        first: 'Primeira',//←
        last: 'Última',//→
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(e, page_num){
        //e.preventDefault();
        $("#results").prepend('<div class="loading-indication"> Loading...</div>');
        $("#pg-results").load("fetch_data.php", {"page": page_num});
    });
});
</script>

<?php require_once '../footer.php'; ?>
```

## Paso 10: Crear el archivo header.php dentro de la carpeta stock

```php
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>stock Simplificado em PHP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>
<br/>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center"><h1><?=$operacao?></h1><h3><a hr```ef='../'>Menu</a></h3></div>
    </div>
```

## Paso 11: Crear el archivo fetch_data.php dentro de la carpeta productos

```php
<?php
include_once("../conexion.php");

if (isset($_POST["page"])) {
    $page_no = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_no))
        die("Error fetching data! Invalid page number!!!");
} else {
    $page_no = 1;
}

// get record starting position
$start = (($page_no-1) * $row_limit);

if($sgbd == 'mysql'){
    $results = $pdo->prepare("SELECT id,descripcion,stock_maximo,stock_minimo FROM productos ORDER BY id LIMIT $start, $row_limit");
}elseif($sgbd == 'pgsql'){
    $results = $pdo->prepare("SELECT id,descripcion,stock_maximo,stock_minimo FROM productos ORDER BY id LIMIT $row_limit OFFSET $start");    
}

$results->execute();

while($row = $results->fetch(PDO::FETCH_ASSOC)) {
    echo "<tr>" . 
    "<td>" . $row['id'] . "</td>" . 
    "<td>" . $row['descripcion'] . "</td>" . 
    "<td>" . $row['stock_minimo'] . "</td>" .
    "<td>" . $row['stock_maximo'] . "</td>";
  ?>
     <td><a href="update.php?id=<?=$row['id']?>"><i class="glyphicon glyphicon-edit" title="Editar"></a></td>
     <td><a onclick="return confirm('Tem certeza de que deseja excluir este cliente ?')" href="delete.php?id=<?=$row['id']?>"><i class="glyphicon glyphicon-remove-circle" title="Excluir"></i></a></td>
<?php
print "
    </tr>";
}
```

## Paso 12: Crear el archivo add.php

```php
<?php require_once '../header.php'; ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h3>productos</h3>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>Descripcion</td><td><input type="text" name="descripcion"></td></tr>
            <tr><td><b>stock mínimo</td><td><input type="text" name="stock_minimo"></td></tr>
            <tr><td><b>stock máximo</td><td><input type="text" name="stock_maximo"></td></tr>
            <tr><td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Cadastrar">&nbsp;&nbsp;&nbsp;
            <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Voltar"></td></tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php

if(isset($_POST['enviar'])){
    $descripcion = $_POST['descripcion'];
    $stock_minimo = $_POST['stock_minimo'];
    $stock_maximo = $_POST['stock_maximo'];

    require_once('../conexion.php');
    try{
       $sql = "INSERT INTO productos(descripcion,stock_minimo,stock_maximo) VALUES (?, ?, ?)";
       $stm = $pdo->prepare($sql)->execute([$descripcion, $stock_minimo, $stock_maximo]);;
 
       if($stm){
           echo 'Dados inseridos com sucesso';
     header('location: index.php');
       }
       else{
           echo 'Erro ao inserir os dados';
       }
   }
   catch(PDOException $e){
      echo $e->getMessage();
   }
}
require_once('../footer.php');
?>
```

## Paso 13: Crear el archivo update.php

```php
<?php require_once('../header.php'); ?>
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form method="post" action="">
                <table class="table table-bordered table-responsive table-hover">

<?php
require_once('../conexion.php');

$id=$_GET['id'];

$sth = $pdo->prepare("SELECT id, descripcion,stock_minimo,stock_maximo from productos WHERE id = :id");
$sth->bindValue(':id', $id, PDO::PARAM_STR); // No select e no delete basta um bindValue
$sth->execute();

$reg = $sth->fetch(PDO::FETCH_OBJ);

$id = $reg->id;
$descripcion = $reg->descripcion;
$stock_minimo = $reg->stock_minimo;
$stock_maximo = $reg->stock_maximo;
?>
   <h3>productos</h3>
   <tr><td>Descripcion</td><td><input name="descripcion" type="text" value="<?=$descripcion?>"></td></tr>
   <tr><td>stock mínimo</td><td><input name="stock_minimo" type="text" value="<?=$stock_minimo?>"></td></tr>
   <tr><td>stock máximo</td><td><input name="stock_maximo" type="text" value="<?=$stock_maximo?>"></td></tr>
   <tr><td></td><td><input name="enviar" class="btn btn-primary" type="submit" value="Editar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <input name="id" type="hidden" value="<?=$id?>">
   <input name="enviar" class="btn btn-warning" type="button" onclick="location='index.php'" value="Regresar"></td></tr>
       </table>
        </form>
        </div>
    <div>
</div>
<?php

if(isset($_POST['enviar'])){
    $id = $_POST['id'];
    $descripcion = $_POST['descripcion'];
    $stock_minimo = $_POST['stock_minimo'];
    $stock_maximo = $_POST['stock_maximo'];

    $data = [
        'descripcion' => $descripcion,
        'stock_minimo' => $stock_minimo,
        'stock_maximo' => $stock_maximo,
        'id' => $id,
    ];

    $sql = "UPDATE productos SET descripcion=:descripcion, stock_minimo=:stock_minimo,stock_maximo=:stock_maximo  WHERE id=:id";
    $stmt= $pdo->prepare($sql);

   if($stmt->execute($data)){
        print "<script>alert('Registro alterado com sucesso!');location='index.php';</script>";
    }else{
        print "Erro ao editar o registro!<br><br>";
    }
}
require_once('../footer.php');
?>
```

## Paso 14: Crear el archivo delete.php

```php
<?php
require_once('../conexion.php');

$id=$_GET['id'];

$sql = "DELETE FROM productos WHERE id = :id";
$sth = $pdo->prepare($sql);
$sth->bindParam(':id', $id, PDO::PARAM_INT);
if( $sth->execute()){
    print "<script>alert('Registro excluído com sucesso!');location='index.php';</script>";
}else{
    print "Erro ao exclur o registro!<br><br>";
}
?>
```

## Paso 15: Crear una carpeta ventas dentro del directorio stock

## Paso 16: Crear el archivo index.php dentro de la carpeta index.php

```php
<?php
include_once("../conexion.php");

$stmt = $pdo->prepare("SELECT COUNT(*) FROM ventas");
$stmt->execute();
$rows = $stmt->fetch();

// get total no. of pages
$total_pages = ceil($rows[0]/$row_limit);

$operacion = 'Ventas';

require_once '../header.php';
?>
        <div class="row">
            <!-- Adicionar registro -->
            <div class="text-left col-md-2 top">
                <a href="./add.php" class="btn btn-primary pull-left">
                    <span class="glyphicon glyphicon-plus"></span> Adicionar
                </a>
            </div>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Produto</th>
                    <th>cantidad</th>
                    <th>Data</th>
                    <th>Preço</th>
                </tr>
            </thead>
            <tbody id="pg-results">
            </tbody>
        </table>
        <div class="panel-footer text-center">
            <div class="pagination"></div>
        </div>
    </div>
</div>
    
<script src="../assets/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.bootpag.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#pg-results").load("fetch_data.php");
    $(".pagination").bootpag({
        total: <?=$total_pages?>,
        page: 1,
        maxVisible: 18,
        leaps: true,
        firstLastUse: true,
        first: 'Primeira',//←
        last: 'Última',//→
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(e, page_num){
        //e.preventDefault();
        $("#results").prepend('<div class="loading-indication"> Loading...</div>');
        $("#pg-results").load("fetch_data.php", {"page": page_num});
    });
});
</script>

<?php require_once '../footer.php'; ?>
```

## Paso 17: Crear el archivo add.php dentro de la carpeta ventas

```php
<?php
require_once '../header.php'; 
require_once('../conexion.php');

// Receber o id e a descripcion de todos os productos para para popular a combo
$sql ='SELECT id,descripcion FROM productos;';
$stmt = $pdo->prepare($sql);
$stmt ->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Pegar o maior id de ventas e somar mais 1 para mostrar no campo id, que é somente leitura
$sqlv ='SELECT MAX(id) AS id FROM ventas;';
$stmtv = $pdo->prepare($sqlv);
$stmtv ->execute();
$venda_id = $stmtv->fetchAll(PDO::FETCH_ASSOC);
$venda_id = $venda_id[0]['id'] + 1;
?>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h3>Compras</h3>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>ID</td><td><input type="text" name="id" value="<?=$venda_id?>" readonly></td></tr>
            <tr><td><b>Produto</td>
            <td>
            <select name="produto_id" id="produto_id">
                <option value="" selected>Selecione</option>
                <?php foreach($data as $row) : ?>
                    <option value="<?= $row['id']; ?>"><?= $row['descripcion']; ?></option>
                <?php endforeach ?>
            </select>
            </td>
            </tr>
            <tr><td><b>cantidad</td><td><input type="text" name="cantidad"></td></tr>
            <tr><td><b>Data</td><td><input type="text" name="data"></td></tr>
            <tr><td><b>Preço</td><td><input type="text" name="precio"></td></tr>
            <tr>
                <td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Registrar">&nbsp;&nbsp;&nbsp;
                    <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Regresar">
                </td>
            </tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php
if(isset($_POST['enviar'])){
    // Receber valores digitados no form
    $produto_id = $_POST['produto_id'];
    $cantidad = $_POST['cantidad'];
    $data = $_POST['data'];
    $precio = $_POST['precio'];

    // Pegar stock_minimo de productos para $produto_id
    $sql ='SELECT stock_minimo AS minimo FROM productos WHERE id = '.$produto_id;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $min = $stmt->fetch(PDO::FETCH_ASSOC);
    $min = $min['minimo'];

    // Pegar a cantidad em stock para $produto_id
    $sql ='SELECT sum(cantidad) AS cantidad FROM stock WHERE id = '.$produto_id;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $soma_stock = $stmt->fetch(PDO::FETCH_ASSOC);
    $soma_stock = $soma_stock['cantidad'];
    
    if(is_null($soma_stock)) {
        $soma_stock = 0;
    }

    if($cantidad > $soma_stock) {
    ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
        <script>
          //  document.getElementById("olamundo").addEventListener("click", exibeMensagem);
            Swal.fire({
              icon: 'error',
              title: 'cantidad muito alta',
              text: 'O stock máximo é de '+"<?=$soma_stock?>",
            })
        // https://blog.betrybe.com/desenvolvimento-web/sweetalert/
        </script>
    <?php
    }else{
        try{
           $sql = "INSERT INTO ventas(produto_id, cantidad, data, precio) VALUES (?,?,?,?)";
           $stm = $pdo->prepare($sql)->execute([$produto_id, $cantidad, $data, $precio]);
           $quant_form = $cantidad;

           if($soma_stock == 0){
               // Adicionar a cantidad comprada ao stock
               $sqle = "INSERT INTO stock(produto_id, cantidad) VALUES (?,?)";
               $stme = $pdo->prepare($sqle)->execute([$produto_id, $cantidad]);
           }else{
               // Atualizar a cantidad comprada no stock
               $stock_atual = $soma_stock - $quant_form;

               $sqle = "UPDATE stock set cantidad = $stock_atual WHERE produto_id = ?";
               $stme = $pdo->prepare($sqle)->execute([$produto_id]);     
           }

           if($stm){
               echo 'Dados inseridos com sucesso';
         header('location: ../stock/index.php');
           }
           else{
               echo 'Erro ao inserir os dados';
           }
       }
       catch(PDOException $e){
          echo $e->getMessage();
       }
    }
}

require_once('../footer.php');
?>
```

## Paso 18: Crear el archivo fetch_data.php

```php
<?php

include_once("../conexion.php");

if (isset($_POST["page"])) {
    $page_no = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_no))
        die("Error fetching data! Invalid page number!!!");
} else {
    $page_no = 1;
}

// get record starting position
$start = (($page_no-1) * $row_limit);

if($sgbd == 'mysql'){
    $results = $pdo->prepare("SELECT id,producto_id,cantidad,data,precio FROM ventas ORDER BY id LIMIT $start, $row_limit");
}elseif($sgbd == 'pgsql'){
    $results = $pdo->prepare("SELECT id,producto_id,cantidad,data,precio FROM ventas ORDER BY id LIMIT $row_limit OFFSET $start");
}

$results->execute();

while($row = $results->fetch(PDO::FETCH_ASSOC)) {
    // Trazer a Descripcion do produto atual
    $sql = 'select descripcion from productos where id = '.$row['producto_id'];
    $stm = $pdo->query($sql);
    $produto = $stm->fetch(PDO::FETCH_OBJ);

    echo "<tr>" .
    "<td>" . $row['id'] . "</td>" .
    "<td>" . $produto->descripcion . "</td>".
    "<td>" . $row['cantidad'] . "</td>" .
    "<td>" . $row['data'] . "</td>" .
    "<td>" . $row['precio'] . "</td>
    </tr>";
}
```

## Paso 19: Crear la carpeta compras dentro del directorio stock

## Paso 20: Crear el archivo index.php

```php
<?php
include_once("../conexion.php");

$stmt = $pdo->prepare("SELECT COUNT(*) FROM compras");
$stmt->execute();
$rows = $stmt->fetch();

// get total no. of pages
$total_pages = ceil($rows[0]/$row_limit);

$operacion = 'Compras';

require_once '../header.php';
?>

        <div class="row">
            <!-- Adicionar registro -->
            <div class="text-left col-md-2 top">
                <a href="./add.php" class="btn btn-primary pull-left">
                    <span class="glyphicon glyphicon-plus"></span> Adicionar
                </a>
            </div>

            <!-- Form de busca-->
     </div>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Produto</th>
                    <th>cantidad</th>
                    <th>Precio</th>
                    <th>Data</th>
                </tr>
            </thead>
            <tbody id="pg-results">
            </tbody>
        </table>
        <div class="panel-footer text-center">
            <div class="pagination"></div>
        </div>
    </div>
</div>
    
<script src="../assets/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.bootpag.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#pg-results").load("fetch_data.php");
    $(".pagination").bootpag({
        total: <?=$total_pages?>,
        page: 1,
        maxVisible: 18,
        leaps: true,
        firstLastUse: true,
        first: 'Primeira',//←
        last: 'Última',//→
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(e, page_num){
        //e.preventDefault();
        $("#results").prepend('<div class="loading-indication"> Loading...</div>');
        $("#pg-results").load("fetch_data.php", {"page": page_num});
    });
});
</script>

<?php require_once '../footer.php'; ?>
```

## Paso 21: Crear el archivo add.php

```php
<?php
require_once '../header.php'; 
require_once('../conexion.php');

$sql ='SELECT id,descripcion FROM productos;';
$stmt = $pdo->prepare($sql);
$stmt ->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

$sqlc ='SELECT MAX(id) AS id FROM compras;';
$stmtc = $pdo->prepare($sqlc);
$stmtc ->execute();
$compra_id = $stmtc->fetchAll(PDO::FETCH_ASSOC);
$compra_id = $compra_id[0]['id'] + 1;
?>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h3>Compras</h3>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>ID</td><td><input type="text" name="id" value="<?=$compra_id?>" readonly></td></tr>
            <tr><td><b>Produto</td>
            <td>
            <select name="producto_id" id="producto_id">
                <option value="" selected>Selecione</option>
                <?php foreach($data as $row) : ?>
                    <option value="<?= $row['id']; ?>"><?= $row['descripcion']; ?></option>
                <?php endforeach ?>
            </select>
            </td>
            </tr>
            <tr><td><b>cantidad</td><td><input type="text" name="cantidad"></td></tr>
            <tr><td><b>Precio</td><td><input type="text" name="precio"></td></tr>
            <tr><td><b>Data</td><td><input type="text" name="data"></td></tr>
            <tr>
                <td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Registrar">&nbsp;&nbsp;&nbsp;
                    <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Regresar">
                </td>
            </tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php
if(isset($_POST['enviar'])){
    // Receber valores digitados no form
    $producto_id = $_POST['producto_id'];
    $cantidad = $_POST['cantidad'];
    $precio = $_POST['precio'];
    $data = $_POST['data'];
    $compra_id = $_POST['id'];

    // Pegar stock_maximo de productos para $producto_id
    $sql ='SELECT stock_maximo AS maximo FROM productos WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt ->execute();
    $max = $stmt->fetch(PDO::FETCH_ASSOC);
    $max = $max['maximo'];

    // Pegar a cantidad em stock para $producto_id
    $sql ='SELECT sum(cantidad) AS cantidad FROM stock WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt ->execute();
    $soma_stock = $stmt->fetch(PDO::FETCH_ASSOC);
    $soma_stock = $soma_stock['cantidad'];
    
    if(is_null($soma_stock)) {
        $soma_stock = 0;
    }

    $quant = $max + $soma_stock;

    if($cantidad > $quant) {
    ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
        <script>
          //  document.getElementById("olamundo").addEventListener("click", exibeMensagem);
            Swal.fire({
              icon: 'error',
              title: 'cantidad muito alta',
              text: 'O stock máximo é de '+"<?=$max?>",
            })
        // https://blog.betrybe.com/desenvolvimento-web/sweetalert/
        </script>
    <?php
    }else{
        try{
           $sql = "INSERT INTO compras(producto_id, cantidad, precio, data) VALUES (?,?,?,?)";
           $stm = $pdo->prepare($sql)->execute([$producto_id, $cantidad, $precio, $data]);
           $quant_form = $cantidad;

           if($soma_stock == 0){
               // Adicionar a cantidad comprada ao stock
               $sqle = "INSERT INTO stock(producto_id, cantidad) VALUES (?,?)";
               $stme = $pdo->prepare($sqle)->execute([$producto_id, $cantidad]);
           }else{
               // Atualizar a cantidad comprada no stock
               $stock_atual = $quant_form + $soma_stock;

               $sqle = "UPDATE stock set cantidad = $stock_atual WHERE producto_id = ?";
               $stme = $pdo->prepare($sqle)->execute([$producto_id]);     
           }

           if($stm){
               echo 'Dados inseridos com sucesso';
         header('location: ../stock/index.php');
           }
           else{
               echo 'Erro ao inserir os dados';
           }
       }
       catch(PDOException $e){
          echo $e->getMessage();
       }
    }
}

require_once('../footer.php');
?>
```

## Paso 22: Crear el archivo fetch_data.php

```php
<?php
include_once("../conexion.php");

if (isset($_POST["page"])) {
    $page_no = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_no))
        die("Error fetching data! Invalid page number!!!");
} else {
    $page_no = 1;
}

// get record starting position
$start = (($page_no-1) * $row_limit);

if($sgbd == 'mysql'){
    $results = $pdo->prepare("SELECT id, producto_id, cantidad, precio, data FROM compras ORDER BY id LIMIT $start, $row_limit");
}elseif($sgbd == 'pgsql'){
    $results = $pdo->prepare("SELECT id, producto_id, cantidad, precio, data FROM compras ORDER BY id LIMIT $row_limit OFFSET $start");    
}
$results->execute();

while($row = $results->fetch(PDO::FETCH_ASSOC)) {
// Trazer a Descripcion do produto atual
$sql = 'select descripcion from productos where id = '.$row['producto_id'];
$stm = $pdo->query($sql);
$produto = $stm->fetch(PDO::FETCH_OBJ);

    echo "<tr>" . 
    "<td>" . $row['id'] . "</td>" . 
    "<td>" . $produto->descripcion . "</td>".
    "<td>" . $row['cantidad'] . "</td>".
    "<td>" . $row['precio'] . "</td>".
    "<td>" . $row['data'] . "</td>
    </tr>";
}
```
