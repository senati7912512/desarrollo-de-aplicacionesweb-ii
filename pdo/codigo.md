# Crear la clase de datos

```sql
CREATE TABLE usuarios (
 id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
 apellido_paterno VARCHAR(30) NOT NULL,
 apellido_materno VARCHAR(30) NOT NULL,
 nombres VARCHAR(30) NOT NULL,
 email VARCHAR(50) NOT NULL,
 edad INT(3),
 procedencia VARCHAR(50),
 fecha TIMESTAMP
);
```
## Crear conexion.php

```php
<?php

$host       = "localhost";
$usuario   = "root";
$contraseña   = "admin";
$base_datos     = "test2";
$dsn        = "mysql:host=$host;dbname=$dbname";
$opciones    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
?>
```

## Crear common.php

```php
<?php

session_start();

if (empty($_SESSION['csrf'])) {
    if (function_exists('random_bytes')) {
        $_SESSION['csrf'] = bin2hex(random_bytes(32));
    } else if (function_exists('mcrypt_create_iv')) {
        $_SESSION['csrf'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    } else {
        $_SESSION['csrf'] = bin2hex(openssl_random_pseudo_bytes(32));
    }
}

function escape($html) {
    return htmlspecialchars($html ?? '');
}

?>
```

## Crear una carpeta 'public' dentro del directorio pdo

## Crear una carpeta 'templates' dentro del directorio pdo

## Crear los arhivos:

## footer.php

```php
</body>

</html>
```

## header.php

```php
<!doctype html>
<html lang="es">

<head>
 <meta charset="utf-8">
 <meta http-equiv="x-ua-compatible" content="ie=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <title>PHP - PDO </title>

 <link rel="stylesheet" href="css/style.css">
</head>

<body>
 <h1>CRUD PHP - PDO</h1>
```

## Crear dentro de public el archivo crear.php

```php
<?php
require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $new_user = array(
      "apellido_paterno" => $_POST['apellido_paterno'],
      "apellido_materno"  => $_POST['apellido_materno'],
      "nombres"  => $_POST['nombres'],
      "email"     => $_POST['email'],
      "edad"       => $_POST['edad'],
      "procedencia"  => $_POST['procedencia']
    );

    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "usuarios",
      implode(", ", array_keys($new_user)),
      ":" . implode(", :", array_keys($new_user))
    );

    $statement = $conexion->prepare($sql);
    $statement->execute($new_user);
  } catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
  <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Agregado exitosamente.</blockquote>
<?php endif; ?>

<h2>Agregar Usuario</h2>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <label for="apellido_paterni">Apellido Paterno</label>
  <input type="text" name="apellido_paterno" id="apellido_paterno">
  <label for="apellido_materno">Apellido Materno</label>
  <input type="text" name="apellido_materno" id="apellido_materno">
  <label for="nombres">Nombres</label>
  <input type="text" name="nombres" id="nombres">
  <label for="email">Email</label>
  <input type="text" name="email" id="email">
  <label for="edad">Edad</label>
  <input type="text" name="edad" id="edad">
  <label for="procedencia">Procedencia</label>
  <input type="text" name="procedencia" id="procedencia">
  <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
```

## Crear el archivo index.php

```php
<?php include "templates/header.php"; ?>

<ul>
 <li><a href="crear.php"><strong>Crear</strong></a> - Crear Usuario</li>
 <li><a href="leer.php"><strong>Leer</strong></a> - Listar usuarios</li>
 <li><a href="actualizar.php"><strong>Actualizar</strong></a> - Editar usuarios</li>
 <li><a href="eliminar.php"><strong>Eliminar</strong></a> - Eliminar usuario</li>
</ul>

<?php include "templates/footer.php"; ?>
```

## Crear el archivo actualizar.php

```php
<?php

require "../conexion.php";
require "../common.php";

try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $sql = "SELECT * FROM usuarios";

    $statement = $conexion->prepare($sql);
    $statement->execute();

    $result = $statement->fetchAll();
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>
<?php require "templates/header.php"; ?>

<h2>Actualizar tabla usuarios</h2>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Nombres</th>
            <th>Email</th>
            <th>Edad</th>
            <th>Procedencia</th>
            <th>Fecha</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $row) : ?>
            <tr>
                <td><?php echo escape($row["id"]); ?></td>
                <td><?php echo escape($row["apellido_paterno"]); ?></td>
                <td><?php echo escape($row["apellido_materno"]); ?></td>
                <td><?php echo escape($row["nombres"]); ?></td>
                <td><?php echo escape($row["email"]); ?></td>
                <td><?php echo escape($row["edad"]); ?></td>
                <td><?php echo escape($row["procedencia"]); ?></td>
                <td><?php echo escape($row["fecha"]); ?> </td>
                <td><a href="base-actualizar.php?id=<?php echo escape($row["id"]); ?>">Editar</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<a href="index.php">Regresar al menu inicio</a>

<?php require "templates/footer.php"; ?>
```

## Crear el archivo base-actualizar.php

```php
<?php

require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $user =[
      "id"        => $_POST['id'],
      "apellido_paterno" => $_POST['apellido_paterno'],
      "apellido_materno" => $_POST['apellido_materno'],
      "nombres"  => $_POST['nombres'],
      "email"     => $_POST['email'],
      "edad"       => $_POST['edad'],
      "procedencia"  => $_POST['procedencia'],
      "fecha"      => $_POST['fecha']
    ];

    $sql = "UPDATE usuarios 
            SET id = :id, 
              apellido_paterno = :apellido_paterno, 
              apellido_materno = :apellido_materno, 
              nombres = :nombres, 
              email = :email, 
              edad = :edad, 
              procedencia = :procedencia, 
              fecha = :fecha 
            WHERE id = :id";
  
  $statement = $conexion->prepare($sql);
  $statement->execute($user);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
  
if (isset($_GET['id'])) {
  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);
    $id = $_GET['id'];

    $sql = "SELECT * FROM users WHERE id = :id";
    $statement = $conexion->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    
    $user = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "¡Algo salió mal!";
    exit;
}
?>

<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
 <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Actualizado exitosamente.</blockquote>
<?php endif; ?>

<h2>Editar Usuario</h2>

<form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <?php foreach ($user as $key => $value) : ?>
      <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
     <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'id' ? 'readonly' : null); ?>>
    <?php endforeach; ?> 
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar al inicio</a>

<?php require "templates/footer.php"; ?>

```

## Crear el archivo leer.php

```php
<?php

require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $sql = "SELECT * 
            FROM usuarios
            WHERE procedencia = :procedencia";

    $procedencia = $_POST['procedencia'];
    $statement = $conexion->prepare($sql);
    $statement->bindParam(':procedencia', $procedencia, PDO::PARAM_STR);
    $statement->execute();

    $result = $statement->fetchAll();
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>
        
<?php  
if (isset($_POST['submit'])) {
  if ($result && $statement->rowCount() > 0) { ?>
    <h2>Results</h2>

    <table>
      <thead>
        <tr>
          <th>#</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Nonbres</th>
          <th>Email</th>
          <th>Edad</th>
          <th>Procedencia</th>
          <th>Fecha</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($result as $row) : ?>
        <tr>
          <td><?php echo escape($row["id"]); ?></td>
          <td><?php echo escape($row["apellido_paterno"]); ?></td>
          <td><?php echo escape($row["apellido_materno"]); ?></td>
          <td><?php echo escape($row["nombres"]); ?></td>
          <td><?php echo escape($row["email"]); ?></td>
          <td><?php echo escape($row["edad"]); ?></td>
          <td><?php echo escape($row["procedencia"]); ?></td>
          <td><?php echo escape($row["fecha"]); ?> </td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
    <?php } else { ?>
      <blockquote>No results found for <?php echo escape($_POST['procedencia']); ?>.</blockquote>
    <?php } 
} ?> 

<h2>Encontrar usuario basado en procedencia</h2>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <label for="procedencia">Procedencia</label>
  <input type="text" id="procedencia" name="procedencia">
  <input type="submit" name="submit" value="result">
</form>

<a href="index.php">Regresar al inicio/a>

<?php require "templates/footer.php"; ?>
```

## Crear el archivo eliminar.php

```php
<?php

require "../conexion.php";
require "../common.php";

$success = null;

if (isset($_POST["submit"])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);
  
    $id = $_POST["submit"];

    $sql = "DELETE FROM users WHERE id = :id";

    $statement = $conexion->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "Usuario eliminado correctamente";
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}

try {
  $conexion = new PDO($dsn, $usuario, $contraseña);

  $sql = "SELECT * FROM usuarios";

  $statement = $conexion->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>
<?php require "templates/header.php"; ?>
        
<h2>Eliminar Usuarios</h2>

<?php if ($success) echo $success; ?>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <table>
    <thead>
      <tr>
        <th>#</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Nombres</th>
        <th>Email</th>
        <th>Edad</th>
        <th>Procedencia</th>
        <th>Fecha</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
      <tr>
        <td><?php echo escape($row["id"]); ?></td>
        <td><?php echo escape($row["apellido_paterno"]); ?></td>
        <td><?php echo escape($row["apellido_materno"]); ?></td>
        <td><?php echo escape($row["nombres"]); ?></td>
        <td><?php echo escape($row["email"]); ?></td>
        <td><?php echo escape($row["edad"]); ?></td>
        <td><?php echo escape($row["procedencia"]); ?></td>
        <td><?php echo escape($row["fecha"]); ?> </td>
        <td><button type="submit" name="submit" value="<?php echo escape($row["id"]); ?>">Eliminar</button></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</form>

<a href="index.php">Regresar al inicio</a>

<?php require "templates/footer.php"; ?>
```

## Crear el archivo login.php

```php
<?php
  
  session_start();

  require "../conexion.php";
  require "../common.php";

  if(!isset($_SESSION['email']) || empty($_SESSION['email'])){
    header('location: login.php');
    exit;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <title>Dashboard</title>
</head>
<body class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card card-body bg-light mt-5">
          <h2>Dashboard <small class="text-muted"><?php echo $_SESSION['email']; ?></small></h2>
          <p>Bienvenido<?php echo $_SESSION['name']; ?></p>
          <p><a href="logout.php" class="btn btn-danger">Logout</a></p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
```

## Crear una carpetas "recursos"

## Crear el archivo index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <title>Dashboard</title>
</head>
<body class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card card-body bg-light mt-5">
          <h2>Dashboard <small class="text-muted">test@test.com</small></h2>
          <p>Bienvenido</p>
          <p><a href="logout.php" class="btn btn-danger">Logout</a></p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
```

## Crear el archivo login.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <title>Account Login</title>
</head>
<body class="bg-primary">
<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body bg-light mt-5">
      <h2>Login</h2>
      <p>Logueado.</p>
      <form action="" method="post">
          <div class="form-group">
              <label>Email:<sup>*</sup></label>
              <input type="text" name="email" class="form-control form-control-lg" value="">
              <span class="invalid-feedback"></span>
          </div>    
          <div class="form-group">
              <label>Password:<sup>*</sup></label>
              <input type="contraseña" name="contraseña" class="form-control form-control-lg">
              <span class="invalid-feedback"></span>
          </div>
          <div class="form-row">
            <div class="col">
              <input type="submit" class="btn btn-success btn-block" value="Login">
            </div>
            <div class="col">
              <a href="registro.php" class="btn btn-light btn-block">No account? Registro</a>
            </div>
        </div>
      </form>
        </div>
      </div>
    </div>
</div>    
</body>
</html>
```

## Crear el archivo registro.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <title>Create An Account</title>
</head>
<body class="bg-primary">
<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body bg-light mt-5">
        <h2>Create An Account</h2>
        <p>Please fill this form to register with us</p>
          <form action="" method="post">
            <div class="form-group">
                <label>Nombre:<sup>*</sup></label>
                <input type="text" name="name" class="form-control form-control-lg" value="">
                <span class="invalid-feedback"></span>
            </div> 
            <div class="form-group">
                <label>Email Address:<sup>*</sup></label>
                <input type="email" name="email" class="form-control form-control-lg" value="">
                <span class="invalid-feedback"></span>
            </div>    
            <div class="form-group">
                <label>Password:<sup>*</sup></label>
                <input type="contraseña" name="contraseña" class="form-control form-control-lg" value="">
                <span class="invalid-feedback"></span>
            </div>
            <div class="form-group">
                <label>Confirm Password:<sup>*</sup></label>
                <input type="contraseña" name="confirm_password" class="form-control form-control-lg" value="">
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-row">
              <div class="col">
                <input type="submit" class="btn btn-success btn-block" value="Register">
              </div>
              <div class="col">
                <a href="login.php" class="btn btn-light btn-block">Have an account? Login</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>    
</body>
</html>

```

## Crear el archivo logout.php

```php
<?php
  
  session_start();
  
  $_SESSION = array();
  
  session_destroy();

  header('location: login.php');
  exit;
  ?>
```
