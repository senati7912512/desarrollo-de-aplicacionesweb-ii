# En esta práctica desarrollaremos un CRUD (Create, Read, Update, Delete) con Angular

## Paso 01: Instalar [node](https://nodejs.org/es)

## Paso 02: Instalar el cliente de angular

```sh
npm install -g @angular/cli
```

## Paso 03: Nos dirigimos a la carpeta raiz www de laragon

```sh
> d:
> cd .\Programas\laragon\www
> code .
```

## Paso 04: Crear el proyecto angular

```sh
ng new veterinaria
```

## Paso 05: Ejecutar el proyecto angular

```sh
ng serve
```

## Paso 06: Crear la base de datos veterinaria

## Paso 07: Crear la base de datos veterinaria

```sql
CREATE TABLE mascotas (
  id bigint(20) UNSIGNED NOT NULL primary key AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  raza varchar(255) NOT NULL,
  edad tinyint(4) DEFAULT NULL
);
```

## Paso 08: Crear el directorio server dentro de la carpeta veterinaria

## Paso 09: Crear el archivo conexion.php dentro de server

```php
<?php

$usuario = "root";
$contraseña = "admin";
$nombre_base_de_datos = "veterinaria";

try {
    echo "Conexion establecida";
    return new PDO('mysql:host=localhost;port=3306;dbname=' . $nombre_base_de_datos, $usuario, $contraseña);
} catch (Exception $e) {
    echo "Ocurrió algo con la base de datos: " . $e->getMessage();
}
?>
```

## Paso 10: Crear el archivo post.php dentro de server

```php
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Headers: *");
$jsonMascota = json_decode(file_get_contents("php://input"));
if (!$jsonMascota) {
    exit("No hay datos");
}
$bd = include_once "conexion.php";
$sentencia = $bd->prepare("insert into mascotas(nombre, raza, edad) values (?,?,?)");
$resultado = $sentencia->execute([$jsonMascota->nombre, $jsonMascota->raza, $jsonMascota->edad]);
echo json_encode([
    "resultado" => $resultado,
]);
```

## Paso 11: Crear el archivo get.php dentro de server

```php
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
if (empty($_GET["idMascota"])) {
    exit("No hay id de mascota");
}
$idMascota = $_GET["idMascota"];
$bd = include_once "conexion.php";
$sentencia = $bd->prepare("select id, nombre, raza, edad from mascotas where id = ?");
$sentencia->execute([$idMascota]);
$mascota = $sentencia->fetchObject();
echo json_encode($mascota);
```

## Paso 12: Crear el archivo getAll.php dentro de server

```php
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
$bd = include_once "conexion.php";
$sentencia = $bd->query("select id, nombre, raza, edad from mascotas");
$mascotas = $sentencia->fetchAll(PDO::FETCH_OBJ);
echo json_encode($mascotas);
```

## Paso 13: Crear el archivo update.php dentro de server

```php
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Headers: *");
if ($_SERVER["REQUEST_METHOD"] != "PUT") {
    exit("Solo acepto peticiones PUT");
}
$jsonMascota = json_decode(file_get_contents("php://input"));
if (!$jsonMascota) {
    exit("No hay datos");
}
$bd = include_once "conexion.php";
$sentencia = $bd->prepare("UPDATE mascotas SET nombre = ?, raza = ?, edad = ? WHERE id = ?");
$resultado = $sentencia->execute([$jsonMascota->nombre, $jsonMascota->raza, $jsonMascota->edad, $jsonMascota->id]);
echo json_encode($resultado);
```

## Paso 14: Crear el archivo delete.php dentro de server

```php
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: DELETE");
$metodo = $_SERVER["REQUEST_METHOD"];
if ($metodo != "DELETE" && $metodo != "OPTIONS") {
    exit("Solo se permite método DELETE");
}

if (empty($_GET["idMascota"])) {
    exit("No hay id de mascota para eliminar");
}
$idMascota = $_GET["idMascota"];
$bd = include_once "conexion.php";
$sentencia = $bd->prepare("DELETE FROM mascotas WHERE id = ?");
$resultado = $sentencia->execute([$idMascota]);
echo json_encode($resultado);
```
